import React from "react";
import * as ReactDOM from "react-dom";
import Tutorial from "./tutorial";

export default function App(){
    return(
        <div>
        <div className="p-16 bg-blue-400 shadow-xl lg:p-8 border-blue-700 border-4 border-opacity-50">
            <h1 className="text-6xl text-center text-blue-700 font-black lg:text-4xl font-marker">This is a Website - Home</h1>
        </div>
        <div className="px-6 py-10 lg:px-20 lg:py-20">
            <div className="textarea">
                A website, also web presence, web appearance, web offer or net appearance, is the - under an individual
                web
                address reachable - presence of a provider of telemedia in the worldwide web (World Wide Web). It is
                created
                with Web techniques, for example HTML, and can be rendered with a user agent, for example a browser.Web
                presence includes Web pages and optionally available downloadable written materials.[1] In other words,
                a
                Web presence is a virtual place on the World Wide Web, usually containing several Web pages, files, and
                other resources that are usually linked to one another by a uniform navigation (using hypertext
                methods).
            </div>
            <h2 className="text-6xl text-blue-400 font-bold px-6 py-24 lg:text-4xl">Here are Buttons:</h2>
            <div className="block text-3xl grid-cols-2 gap-4 text-gray-700 lg:grid lg:text-xl">
                <div className="text-center grid grid-cols-3 p-2">
                    <div className="flex justify-center items-center col-span-2">
                        <p className="text-center">This looks like a normal blue Button -></p>
                    </div>
                    <div className="flex justify-center items-center text-right">
                        <button className="btn stdbtn">
                            Im Jeff
                        </button>
                    </div>
                </div>
                <div className="text-center grid grid-cols-3 p-2">
                    <div className="flex justify-center items-center col-span-2">
                        <p className="text-center">This looks like a normal blue Button -></p>
                    </div>
                    <div className="text-center flex justify-center items-center">
                        <button
                            className="btn bg-green-500 hover:bg-green-400 text-white font-bold py-4 px-12 rounded-lg w-48 shadow-md active:bg-green-700">
                            Im Joe
                        </button>
                    </div>
                </div>
                <div className="text-center grid grid-cols-3 p-2">
                    <div className="flex justify-center items-center col-span-2">
                        <p className="text-center">Seems to be a pill -></p>
                    </div>
                    <div className="text-center flex justify-center items-center">
                        <button
                            className="btn bg-pink-500 hover:bg-pink-400 text-white font-bold py-4 px-12 rounded-full w-48 shadow-md active:bg-pink-700">
                            Im James
                        </button>
                    </div>
                </div>
                <div className="text-center grid grid-cols-3 p-2">
                    <div className="flex justify-center items-center col-span-2">
                        <p className="text-center">Like a normal button with 3D Effects -></p>
                    </div>
                    <div className="text-center">
                        <button
                            className="bg-gray-900 hover:bg-grey-700 text-white font-bold py-4 px-12 border-b-4 border-gray-500 rounded w-48 shadow-md active:bg-black">
                            Im Jerry
                        </button>
                    </div>
                </div>
            </div>
            <h2 className="text-6xl text-blue-400 font-bold px-6 py-24 lg:text-4xl">Here is a collection of the single
                parts of the
                tutorial:</h2>
            <div className="text-3xl text-center text-gray-700 lg:text-xl">
                Go to collection(This Button works):
                <button className="btn stdbtn" onClick={() => ReactDOM.render(<Tutorial/>, document.getElementById("root"))}>
                    Collection
                </button>
            </div>
        </div>
    </div>
    )
}


