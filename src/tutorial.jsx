import React from "react";
import * as ReactDOM from "react-dom";
import App from "./App";
import tutorials from "./data/tutorials";

export default function Tutorial() {
    return (
        <div>
            <div className="p-16 bg-blue-400 shadow-xl lg:p-8 border-blue-700 border-4 border-opacity-50">
                <h1 className="text-6xl text-center text-blue-700 font-black lg:text-4xl font-marker">This is a Website -
                    Tutorial</h1>
            </div>
            <div className="text-3xl px-6 py-10 lg:px-20 lg:py-20 lg:text-xl">
                <button className="btn stdbtn"
                        onClick={() => ReactDOM.render(<App/>, document.getElementById("root"))}>
                    Go Back
                </button>
                <div className="text-gray-700 mt-6">
                    At this point im going to note some things I've learned in the tutorial.
                </div>

                {tutorials.map(tutorial => (
                    <div className="textarea m-12">
                        <h2 className="text-4xl text-blue-400 font-bold mb-6 lg:text-2xl">
                            {tutorial.headline}
                        </h2>
                        <div>
                            {tutorial.text.map(textPart => (
                                <div className={textPart.arg}>
                                    {textPart.content}
                                </div>
                            ))}
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}