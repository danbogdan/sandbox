import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import "./css/tailwind.css";
import Tutorial from "./tutorial";

ReactDOM.render(<App/>, document.getElementById("root"));