export default [
    {
        headline: "Tutorial 1: Setting up Tailwind",
        text:
            [
                {
                    arg: "parea",
                    content: "Setting up Tailwind is easy. You can install Tailwind via npm."
                },
                {
                    arg: "codearea",
                    content: "npm install -D tailwindcss@latest postcss@latest autoprefixer@latest"
                },
                {
                    arg: "parea",
                    content: "After that you can create a config file with:"
                },
                {
                    arg: "codearea",
                    content: "npx tailwindcss init"
                },
                {
                    arg: "parea",
                    content: "When you configured your package.json file you can run the project with:"
                },
                {
                    arg: "codearea",
                    content: "npm run dev"
                }
            ]
    },
    {
        headline: "Tutorial 2: The utility first workflow",
        text:
            [
                {
                    arg: "parea",
                    content: "The utility first workflow describes the way to style the components. You just have to" +
                        " add class parameters to a component you want to style. For example:"
                },
                {
                    arg: "codearea",
                    content: '<div class="bg-gray-8ßß text-gray-300 p-4rounded-2xl shadow-lg></div>"'
                }
            ]
    },
    {
        headline: "Tutorial 3: Responsive Design",
        text:
            [
                {
                    arg: "parea",
                    content: "At Tailwind, the mobile first principle applies. So you design a first a Site for the " +
                        "smallest device. After that you focus on the next bigger device. To do so, there are " +
                        "multiple breakpoints. To make a change on a certain size and bigger you have to choose " +
                        "that breakpoint and write a argument for that. For example:"
                },
                {
                    arg: "codearea",
                    content: "lg:text-xl sm:text-4xl xl:text-sm"
                }
            ]
    },
    {
        headline: "Tutorial 4: Hover, Focus and Active states",
        text:
            [
                {
                    arg: "parea",
                    content: "In TailwindCSS components can have different states(except of the normal state): focus, " +
                        "hover and active. It is possible to set different arguments on certain states. To do that, " +
                        "you need to put the state name in front of the argument."
                },
                {
                    arg: "codearea",
                    content: "hover:bg-blue-300 focus:ring-blue-200 active:bg-blue-700"
                }
            ]
    },
    {
        headline: "Tutorial 5: Separate code with @apply",
        text:
            [
                {
                    arg: "parea",
                    content: "It is possible to avoid dublicated code with using the @apply feature. You can " +
                        "separate some TailwindCSS Code from the HTML File by putting it in the tailwind.css " +
                        "file and refer to this block of code. How it could look like in a HTML file:"
                },
                {
                    arg: "codearea",
                    content: ".btn {\n  @apply hover:-translate-y-1 transform transition focus:outline-none " +
                        "focus:ring focus-within:ring-blue-600\n  focus:ring-opacity-70 " +
                        "focus-within:ring-offset-1\n}\n.stdbtn {\n  @apply bg-blue-500 hover:bg-blue-400 " +
                        "font-bold py-4 px-12 rounded-lg active:bg-blue-700 shadow-md text-white\n}\n"
                }
            ]
    },
    {
        headline: "Tutorial 6: Extracting Reusable Components",
        text:
            [
                {
                    arg: "parea",
                    content: "You can reduce duplicated code using JavaScript. It is possible to store the data in json" +
                        " files and load them with JavaScript into your HTML file."
                },
                {
                    arg: "codearea",
                    content: "{tutorial.text.map(textPart => (<div className={textPart.arg}>{textPart.content}</div>))}"
                }
            ]
    },
    {
        headline: "Tutorial 7: Tailwind configuration",
        text:
            [
                {
                    arg: "parea",
                    content: "It is possible to customize tailwind. For that you can use a full configuration file(which " +
                        "is not recommended) or use the default config file. There you can add new parameter to " +
                        "overwrite or extend the tailwind settings."
                },
                {
                    arg: "codearea",
                    content: "extend: {\n" +
                        "      colors: {\n" +
                        "        lime: {\n" +
                        "          DEFAULT: '#9dc450'\n" +
                        "        }\n" +
                        "      }\n" +
                        "}"
                }
            ]
    },
    {
        headline: "Tutorial 8: Optimize build",
        text:
            [
                {
                    arg: "parea",
                    content: "In the Tailwind config file you can set your build options. There it is possible to run purge to reduce the css File size."
                },
                {
                    arg: "codearea",
                    content: "purge: [\"./src/**/*.jsx\", \"./index.html\"]"
                }
            ]
    }
]